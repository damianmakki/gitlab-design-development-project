$(document).ready(function() {

    $("form[name='register']").validate({

        rules: {
            name: {
                required: true,
                minlength: 2,

            },
            email: {
                required: true,
                email: true,
                minlength: 5
            }
        },
        messages: {
            name: {
                required: "Please enter your name",
                minlength: "Your name must be at least 2 characters long"
            },
            email: {
                required: "Please enter your email address",
                email: "Please enter a valid email address",
                minlength: "Your email address must be at least 4 characters long"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});