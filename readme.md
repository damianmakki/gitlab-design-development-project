# GitLab Design/Development Challenge

## Brief
>>>
Design and develop a landing page promoting an upcoming GitLab webcast. 

The only requirement is to create a sign up form which includes client side validation so that users can receive reminders, and follow up emails after the webcast. 

The rest is up to you.
>>>

## Viewing and Testing

You can find a live version of this project at https://makki.pro/gitlab.

This project has been tested on:

- [x] Chrome (latest)
- [x] Firefox (latest)
- [x] Safari (latest)
- [x] Chrome for iOS (latest)
- [x] Safari for iOS (latest)


## Mockups

You can find an early mockup version of this landing page in Sketch in the `/sketch` folder.